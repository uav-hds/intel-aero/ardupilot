#you can run a prebuilt image with:
#sudo docker run -it -v ~/git/intel-aero/ardupilot/:/home/user/ardupilot --name ardupilot4 -h ardupilot4 registry.gitlab.utc.fr/uav-hds/intel-aero/ardupilot:copter-4-0 /entrypoint.sh bash
#
#otherwise build and run it:
#build container with: sudo docker build -t ardupilot4 .
#run with: sudo docker run -it -v ~/git/intel-aero/ardupilot/:/home/user/ardupilot --name ardupilot4 -h ardupilot4 ardupilot4 /entrypoint.sh bash
#~/git/intel-aero/ardupilot should contain ardupilot 4.0.7:
#git clone https://github.com/ArduPilot/ardupilot.git ardupilot4
#git checkout Copter-4.0.7
#git submodule update --init --recursive

#inside container:
#./waf configure --board aerofc
#./waf copter
#the firmware file will be build/aerofc/bin/arducopter.apj

#to compile an example ./waf build --target examples/UART_test

FROM ubuntu:18.04

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN useradd -ms /bin/bash user

RUN apt-get update && apt-get upgrade -y && apt-get install wget sudo lsb-release python -y

COPY Tools/environment_install/install-prereqs-ubuntu.sh /
#do not init git submodules, we are not in the ardupilot directory
RUN sed -i 's|git submodule|#git submodule|g' /install-prereqs-ubuntu.sh
#change ~/.profile to $USER/.profile
RUN sed -i 's|~/.profile|/home/$USER/.profile|g' /install-prereqs-ubuntu.sh
#as of 09/12/24, MAVProxy cannot be installed, it gives the following error "No matching distribution found for pynmeagps (from MAVProxy)"
#MAVProxy is not necessary to build firmware, so don't install it as a quick fix
RUN sed -i 's|future lxml pymavlink MAVProxy pexpect|future lxml pymavlink pexpect|g' ./install-prereqs-ubuntu.sh

RUN export USER=user && /install-prereqs-ubuntu.sh -y
RUN rm /install-prereqs-ubuntu.sh
RUN su - user -c "pip install future" 

#create entry point script
RUN echo "#!/bin/sh \n"  \
    ". /home/user/.profile \n" \
    "echo configure with ./waf configure --board aerofc \n" \
    "echo compile with ./waf copter \n" \
    "exec \"\$@\"" > /entrypoint.sh 
RUN chmod +x /entrypoint.sh 

USER user
WORKDIR /home/user/ardupilot
