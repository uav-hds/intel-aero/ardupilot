#pragma once

#include "AP_BattMonitor.h"
#include "AP_BattMonitor_Backend.h"
#include <AP_HAL/I2CDevice.h>

class AP_BattMonitor_Aerofc : public AP_BattMonitor_Backend
{
public:

    /// Constructor
    AP_BattMonitor_Aerofc(AP_BattMonitor &mon, AP_BattMonitor::BattMonitor_State &mon_state, AP_BattMonitor_Params &params);

    /// Read the battery voltage and current.  Should be called at 10hz
    void read() override;

    /// returns true if battery monitor provides consumed energy info
    bool has_consumed_energy() const override { return has_current(); }

    /// returns true if battery monitor provides current info
    bool has_current() const override  { return false; }

    void init(void) override;

private:
    AP_HAL::OwnPtr<AP_HAL::I2CDevice> _dev;
    bool need_init;
    void timer(void);
    AP_HAL::Device::PeriodicHandle timer_handle;

protected:

};
