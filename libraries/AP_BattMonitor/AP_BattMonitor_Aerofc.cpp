#include <AP_HAL/AP_HAL.h>

#if CONFIG_HAL_BOARD_SUBTYPE == HAL_BOARD_SUBTYPE_CHIBIOS_AEROFC


#include <AP_Common/AP_Common.h>
#include "AP_BattMonitor.h"
#include "AP_BattMonitor_Aerofc.h"
#include <GCS_MAVLink/GCS.h>

#define I2C_BUS 2
#define SLAVE_ADDR 0x50
#define ADC_ENABLE_REG 0x00
#define ADC_CHANNEL_REG 0x05

// base voltage scaling for 12 bit 3.3V ADC
#define VOLTAGE_SCALING (3.3f/4096.0f)

// multiplier from analog battmonitor of ardupilot 3.6.12
#define BATT_VOLT_MULT 9.116f


extern const AP_HAL::HAL& hal;

/// Constructor
AP_BattMonitor_Aerofc::AP_BattMonitor_Aerofc(AP_BattMonitor &mon,
                                             AP_BattMonitor::BattMonitor_State &mon_state,
                                             AP_BattMonitor_Params &params) :
    AP_BattMonitor_Backend(mon, mon_state, params)
{
   _dev=hal.i2c_mgr->get_device(I2C_BUS, SLAVE_ADDR,400000, false, 20);//20: timeout_ms
    need_init=true;

    // always healthy
    _state.healthy = true;
}

void AP_BattMonitor_Aerofc::init(void) 
{
    //need to set a callback for using i2c, otherwise i2c semaphore is not taken by the good thread
    if (_dev) {
        timer_handle = _dev->register_periodic_callback(100000, FUNCTOR_BIND_MEMBER(&AP_BattMonitor_Aerofc::timer, void));
    }
}

void AP_BattMonitor_Aerofc::timer(void) {
    if(need_init) {
        bool ret;

        /* Enable ADC */
        ret=_dev->write_register(ADC_ENABLE_REG,0x01);
        if (!ret) {
            gcs().send_text(MAV_SEVERITY_WARNING, "AP_BattMonitor_Aerofc: Failed to initialize");
        }

        need_init=false;
        return;
    }

    if(!need_init) {
        bool ret;
        uint8_t buffer[2];

        ret = _dev->read_registers(ADC_CHANNEL_REG, buffer, sizeof(buffer));
        if (!ret) {
            _state.voltage = 0;
            gcs().send_text(MAV_SEVERITY_WARNING, "AP_BattMonitor_Aerofc: Failed to read");
		    return;
	    }
        /* fillup battery state */
        _state.voltage = (buffer[0] | (buffer[1] << 8))*VOLTAGE_SCALING*BATT_VOLT_MULT;
        _state.last_time_micros = AP_HAL::micros();
    }
}

// read - read the voltage and current
void AP_BattMonitor_Aerofc::read()
{
// nothing to be done here for actually interacting with the battery
}

#endif
